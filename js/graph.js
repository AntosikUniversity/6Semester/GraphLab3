class Graph {
  constructor(canvas) {
    this.ctx = canvas.getContext("2d");

    this._edges = [];
    this._loops = [];
    this._vertexes = [];
  }

  get edges() {
    return this._edges;
  }

  get loops() {
    return this._loops;
  }

  get vertexes() {
    return this._vertexes;
  }

  /* VERTEXES */
  createVertex(x, y) {
    const currentVertex = this.findCurrentVertex(x, y);

    if (!currentVertex) {
      this._vertexes.push(new Vertex(x, y, this._vertexes.length));
    }

    this.redraw();
  }

  moveVertex(vertex, { x, y }) {
    vertex.x = x;
    vertex.y = y;

    this.redraw();
  }

  /* VERTEXES */

  /* LOOPS */
  createLoop(x, y) {
    const currentVertex = this.findCurrentVertex(x, y);

    if (currentVertex) {
      this.toggleLoop(currentVertex);
    }
  }

  toggleLoop(vertex, removeIfExists = true) {
    const loopOnVertex = this._loops.find(loop => loop.vertex === vertex);

    if (loopOnVertex && removeIfExists) {
      this._loops = this._loops.filter(loop => loop !== loopOnVertex);
    } else if (!loopOnVertex) {
      this._loops.push(new Loop(vertex));
    }

    this.redraw();
  }

  loopOnVertex(vertex) {
    return this._loops.find(loop => loop.vertex === vertex);
  }

  /* LOOPS */

  /* EDGES */
  createEdge(vertex1, vertex2, weight) {
    this._edges.push(new Edge(vertex1, vertex2, weight));
    this.redraw();
  }

  edgeVertexesConnected(vertex1, vertex2) {
    return this._edges.find(edge =>
      (edge.vertex1 === vertex1 && edge.vertex2 === vertex2) || (edge.vertex2 === vertex1 && edge.vertex1 === vertex2)
    );
  }

  /* EDGES */

  /* HELPERS */
  findCurrentVertex(x, y) {
    return this._vertexes.find(vertex => vertex.isIn(x, y));
  }

  build(vertexes) {
    if (vertexes.length < this._vertexes.length) {
      this._vertexes = this._vertexes.slice(0, vertexes.length)
    } else if (vertexes.length > this._vertexes.length) {
      for (let i = this._vertexes.length, len = vertexes.length; i < len; i++) {
        const x = (Math.random() * (canvas.width - 40)) + 10;
        const y = (Math.random() * (canvas.height - 40)) + 10;

        this.createVertex(x, y);
      }
    }

    return this.rebuild(vertexes);
  }

  rebuild(vertexes) {
    this._edges = [];
    this._loops = [];

    for (let i = 0, len = vertexes.length; i < len; i++) {
      for (let j = 0; j < len; j++) {
        if (vertexes[i][j] !== 0) {
          if (i === j) {
            this.toggleLoop(this._vertexes[i], false);
          } else {
            this.createEdge(this._vertexes[i], this._vertexes[j], vertexes[i][j]);
          }
        }
      }
    }

    return vertexes;
  }

  dijkstra(sourceVertex, targetVertex) {
    const vectors = new Set();
    const prev = {};
    const dist = {};
    const adj = {};

    const source = sourceVertex.index.toString();
    const target = targetVertex.index.toString();

    // Init variables
    for (let i = 0; i < this._edges.length; i++) {
      let { vertex1: { index: v1 }, vertex2: { index: v2 }, weight } = this._edges[i];

      v1 = v1.toString();
      v2 = v2.toString();

      vectors.add(v1);
      vectors.add(v2);

      dist[v1] = Infinity;
      dist[v2] = Infinity;

      if (adj[v1] === undefined) adj[v1] = {};
      if (adj[v2] === undefined) adj[v2] = {};

      adj[v1][v2] = weight;
      adj[v2][v1] = weight;
    }

    /// Function for compare distance
    const vertexWithMinDist = (vectors, dist) => {
      let min = Infinity;
      let vectorWithMin = null;

      for (let vector of vectors) {
        if (dist[vector] < min) {
          min = dist[vector];
          vectorWithMin = vector;
        }
      }

      return vectorWithMin
    };

    dist[source] = 0;

    // Main
    while (vectors.size) {
      let vector = vertexWithMinDist(vectors, dist);
      let neighbors = Object.keys(adj[vector]).filter(v => vectors.has(v));  // Neighbors still exist

      vectors.delete(vector);

      if (vector === target) break;                                          // Break when the target has been found

      for (let v of neighbors) {
        let alt = dist[vector] + adj[vector][v];
        if (alt < dist[v]) {
          dist[v] = alt;
          prev[v] = vector;
        }
      }
    }

    // End
    let u = target;
    let path = [u];
    let len = 0;

    while (prev[u] !== undefined) {
      path.unshift(prev[u]);
      len += adj[u][prev[u]];
      u = prev[u];
    }

    return [path, len];
  }

  /* HELPERS */

  /* CORE */
  highlight(edges = [], vertexes = [], loops = []) {
    Loop.initHighlightStyle(this.ctx);
    loops.forEach(loop => {
      loop.draw(this.ctx);
    });

    Edge.initHighlightStyle(this.ctx);
    edges.forEach(edge => {
      edge.draw(this.ctx);
    });

    Vertex.initHighlightStyle(this.ctx);
    vertexes.forEach(vertex => {
      vertex.draw(this.ctx);
    });
  }

  redraw() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

    Loop.initStyle(this.ctx);
    this._loops.forEach(loop => {
      loop.draw(this.ctx);
    });

    Edge.initStyle(this.ctx);
    this._edges.forEach(edge => {
      edge.draw(this.ctx);
    });

    Vertex.initStyle(this.ctx);
    this._vertexes.forEach(vertex => {
      vertex.draw(this.ctx);
    });
  }

  reset() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this._loops = [];
    this._edges = [];
    this._vertexes = [];
  }

  /* CORE */
}

class Vertex {
  get index() {
    return this._index;
  }

  static radius() {
    return 5
  };

  static initStyle(ctx) {
    ctx.strokeStyle = "#000";
    ctx.lineJoin = "round";
    ctx.lineWidth = 5;
    ctx.font = 'bold 14px serif';
  }

  static initHighlightStyle(ctx) {
    ctx.strokeStyle = "#FFF";
    ctx.lineJoin = "round";
    ctx.lineWidth = 5;
    ctx.font = 'bold 14px serif';
  }

  constructor(x, y, index) {
    this._x = x;
    this._y = y;
    this._index = index;
  }

  draw(ctx) {
    ctx.beginPath();
    ctx.arc(this._x, this._y, Vertex.radius(), 0, 2 * Math.PI, false);
    ctx.closePath();
    ctx.stroke();
    ctx.fillText(this._index, this._x - 4, this._y + 20);
  }

  distanceTo(x, y) {
    const distX = Math.abs(x - this._x);
    const distY = Math.abs(y - this._y);
    return Math.sqrt(distX * distX + distY * distY);
  }

  isIn(x, y) {
    return this.distanceTo(x, y) < (Vertex.radius() + 5);
  }


  get y() {
    return this._y;
  }

  set y(value) {
    this._y = value;
  }

  get x() {
    return this._x;
  }

  set x(value) {
    this._x = value;
  }
}

class Loop {
  static radiusX() {
    return 8
  };

  static radiusY() {
    return 10
  };

  static initStyle(ctx) {
    ctx.strokeStyle = "#2196F3";
    ctx.lineJoin = "round";
    ctx.lineWidth = 2;
  }

  static initHighlightStyle(ctx) {
    ctx.strokeStyle = "#F22121";
    ctx.lineJoin = "round";
    ctx.lineWidth = 2;
  }

  constructor(vertex) {
    this._vertex = vertex;
  }

  draw(ctx) {
    ctx.beginPath();
    ctx.ellipse(
      this._vertex.x + Vertex.radius(),
      this._vertex.y + Vertex.radius(),
      Loop.radiusX(),
      Loop.radiusY(),
      45 * Math.PI / 180, 0, 2 * Math.PI);
    ctx.closePath();
    ctx.stroke();
  }

  get vertex() {
    return this._vertex;
  }
}

class Edge {
  static initStyle(ctx) {
    ctx.strokeStyle = "#2196F3";
    ctx.lineJoin = "round";
    ctx.lineWidth = 4;
    ctx.font = '14px serif';
  }

  static initHighlightStyle(ctx) {
    ctx.strokeStyle = "#F22121";
    ctx.lineJoin = "round";
    ctx.lineWidth = 4;
    ctx.font = '14px serif';
  }

  constructor(vertex1, vertex2, weight = 1) {
    this._weight = weight;
    this._vertex1 = vertex1;
    this._vertex2 = vertex2;
  }

  draw(ctx) {
    ctx.beginPath();

    ctx.moveTo(this.vertex1.x, this.vertex1.y);
    ctx.lineTo(this.vertex2.x, this.vertex2.y);

    ctx.closePath();
    ctx.stroke();

    const textX = (this.vertex1.x + this.vertex2.x) / 2;
    const textY = (this.vertex1.y + this.vertex2.y) / 2;
    ctx.fillText(this._weight, textX + 10, textY);
  }

  get vertex1() {
    return this._vertex1;
  }

  set vertex1(value) {
    this._vertex1 = value;
  }

  get vertex2() {
    return this._vertex2;
  }

  set vertex2(value) {
    this._vertex2 = value;
  }

  get weight() {
    return this._weight;
  }

  set weight(value) {
    this._weight = Number(value) ? value : 0;
  }
}